package com.usuario.rssmysql.interfaces;

import android.view.View;

/**
 * Created by usuario on 13/02/17.
 */

public interface ClickListener {
    void onClick(View view, int position);
    void onLongClick(View view, int position);
}
